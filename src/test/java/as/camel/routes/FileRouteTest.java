package as.camel.routes;

import as.camel.springconfig.CamelConfig;
import as.camel.springconfig.SpringConfig;
import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.AdviceWithRouteBuilder;
import org.apache.camel.test.spring.CamelSpringRunner;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

@RunWith(CamelSpringRunner.class)
@ContextConfiguration(classes = {SpringConfig.class, CamelConfig.class})
public class FileRouteTest {

    @Autowired
    private CamelContext context;

    @Autowired
    private ProducerTemplate producer;

    @Before
    public void setUp() {
    }

    @Test
    public void whenSendingToInbox_thenMessageGoesToOutboxAndArchive() throws Exception {
        context.getRouteDefinition(FileRoute.ROUTE_ID).adviceWith(context, new AdviceWithRouteBuilder() {
            @Override
            public void configure() throws Exception {
                mockEndpoints("*archive", "*outbox");
            }
        });
        context.startRoute(FileRoute.ROUTE_ID);
    }
}