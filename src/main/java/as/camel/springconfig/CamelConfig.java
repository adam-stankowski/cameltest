package as.camel.springconfig;

import org.apache.camel.spring.javaconfig.CamelConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("as.camel.routes")
public class CamelConfig extends CamelConfiguration {

}
