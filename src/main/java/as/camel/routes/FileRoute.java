package as.camel.routes;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
class FileRoute extends RouteBuilder {

    static final String ROUTE_ID = "TestFileRoute";

    @Override
    public void configure() {
        from("file:/inbox").id(ROUTE_ID)
                .wireTap("file:/archive")
                .to("file:/outbox");
    }
}
