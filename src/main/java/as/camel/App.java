package as.camel;

import as.camel.springconfig.SpringConfig;
import org.apache.camel.spring.javaconfig.Main;

public class App {
    public static void main(String[] args) throws Exception {
        Main main = new Main();
        main.setConfigClass(SpringConfig.class);
        main.run();
    }
}
